import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IotInterfaceComponent } from './iot-interface/iot-interface.component';
import { IotDashboardComponent } from './iot-dashboard/iot-dashboard.component';

const routes: Routes = [
  { path: '', redirectTo: '/iotinterface', pathMatch: 'full' },
  { path: 'iotinterface', component: IotInterfaceComponent },
  { path: 'dashboard', component: IotDashboardComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {useHash: true}) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}