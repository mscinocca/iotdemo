import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { IotInterfaceComponent } from './iot-interface/iot-interface.component';
import { IotDashboardComponent } from './iot-dashboard/iot-dashboard.component';

import { SignalRModule } from 'ng2-signalr';
import { SignalRConfiguration } from 'ng2-signalr';

import { AppRoutingModule } from './app-routing.module';

import { DeviceService } from './services/device.service';
import { DateTimeService } from './services/date-time.service';

import './libraries/rxjs-extensions';
import { SignalRService } from './services/signalr.service';

export function createConfig(): SignalRConfiguration {
  const c = new SignalRConfiguration();
  c.hubName = 'iotHub';
  c.url = '/IoTDemo';
  c.logging = false;
  return c;
}

@NgModule({
  declarations: [
    AppComponent,
    IotInterfaceComponent,
    IotDashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    SignalRModule.forRoot(createConfig),
  ],
  providers: [ 
    DeviceService,
    DateTimeService,
    SignalRService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
