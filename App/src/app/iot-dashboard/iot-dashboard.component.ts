import { Component, OnInit } from '@angular/core';
import { SignalRService } from '../services/signalr.service';
import { Telemetry } from '../models/telemetry';
import { DateTimeService } from '../services/date-time.service';
import { IoTDevice } from '../models/iot-device';

declare var jQuery:any;

declare var google:any;

@Component({
  selector: 'app-iot-dashboard',
  templateUrl: './iot-dashboard.component.html',
  styleUrls: ['./iot-dashboard.component.css']
})
export class IotDashboardComponent implements OnInit {

  selectedDevice: IoTDevice;
  deviceList: Array<IoTDevice>;

  telemetryList: Map<string, Array<Telemetry>>;

  markerList: Map<string, any>;

  lineChartInstance: any;
  lineChartData: any = [];

  map: any;

  constructor(
    private signalRService: SignalRService,
    private dateTimeService: DateTimeService) {
      this.deviceList = new Array<IoTDevice>();

      this.markerList = new Map<string, any>();

      this.telemetryList = new Map<string, Array<Telemetry>>();
  }

  ngOnInit() {
    this.signalRService.onTelemetryUpdate$
      .subscribe((telemetry: Telemetry) => {

        var deviceListIndex = this.deviceList.findIndex(x => x.DeviceId == telemetry.DeviceId);

        if(deviceListIndex < 0) {

          var device = new IoTDevice();
          device.DeviceId = telemetry.DeviceId;
          device.CurrentTemperature = telemetry.Temperature;
          device.CurrentSeismicActivity = telemetry.SeismicActivity;

          this.deviceList.push(device);

          deviceListIndex = this.deviceList.length - 1;

          var marker = this.plotRandomMarker(device.DeviceId);

          this.markerList.set(device.DeviceId, marker);
        }

        this.deviceList[deviceListIndex].CurrentTemperature = telemetry.Temperature;
        this.deviceList[deviceListIndex].CurrentSeismicActivity = telemetry.SeismicActivity;

        var marker = this.markerList.get(this.deviceList[deviceListIndex].DeviceId);
        marker.icon.scale = Math.max(5, telemetry.SeismicActivity);

        marker.setMap(this.map);

      /*
      if(!this.telemetryList.has(telemetry.IoTDevice.DeviceId)) {
        this.telemetryList.set(telemetry.IoTDevice.DeviceId, []);
      }
      */

      telemetry.DateTime = this.dateTimeService.reviver.call("", "", telemetry.DateTime);

      //this.telemetryList.get(telemetry.IoTDevice.DeviceId).push(telemetry);

      if(this.selectedDevice != null && telemetry.DeviceId == this.selectedDevice.DeviceId) {

        this.lineChartInstance.push([
          { time: Math.floor(telemetry.DateTime.getTime() / 1000), y: telemetry.Temperature },
          { time: Math.floor(telemetry.DateTime.getTime() / 1000), y: telemetry.SeismicActivity }
        ]);
      }
    });

    this.lineChartInstance = jQuery('#area').epoch({
      type: 'time.line',
      data: this.lineChartData,
      axes: ['left', 'bottom'],
      margins: { left: 24 },
      queueSize: 1,
      historySize: 1
    });

    this.initializeChart();

    this.map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: 43.651647, lng: -79.493545},
      zoom: 12
    });
  }

  plotRandomMarker(deviceId: string) {

    var point = new google.maps.LatLng(this.randomBetween(43.651647 - 0.05, 43.651647 + 0.05), this.randomBetween(-79.493545 - 0.05, -79.493545 + 0.05));

    var marker = new google.maps.Marker({
      position: point,
      icon: {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 10
      },
      label: deviceId,
      draggable: false,
      map: this.map
    });

    return marker;
  }

  randomBetween(min, max) {
    return Math.random() * (max - min) + min;
  }

  initializeChart() {
    this.lineChartData = [
      { label: 'T', values: [] },
      { label: 'S', values: [] },
    ];

    this.lineChartInstance.update(this.lineChartData);
  }

  selectDevice(device: IoTDevice) {
    this.selectedDevice = device;

    this.initializeChart();
  }
}
