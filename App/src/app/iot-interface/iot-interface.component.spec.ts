import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IotInterfaceComponent } from './iot-interface.component';

describe('IotInterfaceComponent', () => {
  let component: IotInterfaceComponent;
  let fixture: ComponentFixture<IotInterfaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IotInterfaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IotInterfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
