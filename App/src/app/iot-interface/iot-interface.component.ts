import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../services/device.service';
import { IoTDevice } from '../models/iot-device';
import { Telemetry } from '../models/telemetry';

declare var jQuery:any;

@Component({
  selector: 'app-iot-interface',
  templateUrl: './iot-interface.component.html',
  styleUrls: ['./iot-interface.component.css']
})

export class IotInterfaceComponent implements OnInit {

  iotDevice: IoTDevice;

  temperature: number;

  seismicActivity: number;

  lineChartInstance: any;
  lineChartData: any = [];

  telemetry: Telemetry;
  
  lastDateSent: Date;

  constructor(private deviceService: DeviceService) { 
    this.temperature = 23;

    this.seismicActivity = 0;

    this.telemetry = new Telemetry();

    this.lastDateSent = new Date();
  }

  ngOnInit() {

    this.lineChartData = [
      { label: 'T', values: [] },
      { label: 'S', values: [] }
    ];

    window.addEventListener("devicemotion", this.handleOrientation.bind(this), true);

    //setInterval(this.fakeData.bind(this), 100);

    this.deviceService.registerDevice().subscribe(x => this.iotDevice = x);
  }

  ngAfterViewInit(){
    this.lineChartInstance = jQuery('#area').epoch({
      type: 'time.line',
      data: this.lineChartData,
      axes: ['left', 'bottom'],
      margins: { left: 24 },
      queueSize: 1,
      historySize: 1
    });
  }

  raiseTemperature() {
    this.temperature++;
  }

  lowerTemperature() {
    this.temperature--;
  }

  handleOrientation(event) {
    var nowDate = new Date();

    if(event.acceleration.x != null) {
      if(nowDate.getTime() - this.lastDateSent.getTime() >= 1000) {
        var aX = Math.round(event.acceleration.x);
        var aY = Math.round(event.acceleration.y);
        var aZ = Math.round(event.acceleration.z);

        var date = Math.floor(new Date().getTime() / 1000);

        this.seismicActivity = Math.round(Math.sqrt(aX * aX + aY * aY + aZ * aZ));

        this.lineChartInstance.push([{ time: date, y: this.temperature }, { time: date, y: this.seismicActivity }]);

        this.telemetry.DeviceId = this.iotDevice.DeviceId;
        this.telemetry.DateTime = nowDate;
        this.telemetry.Temperature = this.temperature;
        this.telemetry.SeismicActivity = this.seismicActivity;

        this.deviceService.postTelemetry(this.telemetry).subscribe();

        this.lastDateSent = nowDate;
      }
    } else {
      setInterval(this.fakeData.bind(this), 100);
    }
  }

  fakeData() {
    var nowDate = new Date();

    if(nowDate.getTime() - this.lastDateSent.getTime() >= 1000) {
      var date = Math.floor(nowDate.getTime() / 1000);

      this.seismicActivity = Math.round(Math.random() * 15);

      this.lineChartInstance.push([{ time: date, y: this.temperature }, { time: date, y: this.seismicActivity }]);

      this.telemetry.DeviceId = this.iotDevice.DeviceId;
      this.telemetry.DateTime = nowDate;
      this.telemetry.Temperature = this.temperature;
      this.telemetry.SeismicActivity = this.seismicActivity;

      this.deviceService.postTelemetry(this.telemetry).subscribe();

      this.lastDateSent = nowDate;
    }
  }
}
