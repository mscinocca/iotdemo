import { IoTDevice } from "./iot-device";

export class Telemetry {
    DeviceId: string;
    
    DateTime: Date;
    Temperature: number;
    SeismicActivity: number;
}