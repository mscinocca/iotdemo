import { Injectable } from '@angular/core';

@Injectable()
export class DateTimeService {

  	constructor() { }
	
	dateFormat: RegExp = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}$/;
	dateFormatOffset: RegExp = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}-\d{2}:\d{2}$/;
	dateFormatUTC: RegExp = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/;
	
	reviver = (key, value) => {
		if (typeof value === "string" && this.dateFormat.test(value)) {
			var valueData = value.split(/\D+/);

			return new Date(
				parseInt(valueData[0]),
				parseInt(valueData[1]) - 1,
				parseInt(valueData[2]),
				parseInt(valueData[3]),
				parseInt(valueData[4]),
				parseInt(valueData[5]));

		} else if (typeof value === "string" && this.dateFormatOffset.test(value)) {
			var valueData = value.split(/\D+/);
			
			return new Date(
				parseInt(valueData[0]),
				parseInt(valueData[1]) - 1,
				parseInt(valueData[2]),
				parseInt(valueData[3]),
				parseInt(valueData[4]),
				parseInt(valueData[5]));

		} else if (typeof value === "string" && this.dateFormatUTC.test(value)) {
			var valueData = value.split(/\D+/);

			return new Date(
				Date.UTC(
					parseInt(valueData[0]),
					parseInt(valueData[1]) - 1,
					parseInt(valueData[2]),
					parseInt(valueData[3]),
					parseInt(valueData[4]),
					parseInt(valueData[5])));
		}
		
		return value;
	}

	monthNames = [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"];

	monthNamesShort = [
		"Jan",
		"Feb",
		"Mar",
		"Apr",
		"May",
		"June",
		"July",
		"Aug",
		"Sept",
		"Oct",
		"Nov",
		"Dec"];

	getTimeString(date: Date): string {
		if(date == null) {
			return "";
		}
	
		return ("0" + date.getHours()).slice(-2) + ":" + ("0" + date.getMinutes()).slice(-2) + ":" + ("0" + date.getSeconds()).slice(-2);
	}
	
	getShortDateString(date: Date): string {
		if(date == null) {
			return "";
		}
	
		return this.monthNamesShort[date.getMonth()] + " " + date.getDate() + " " + date.getFullYear();
	}

	getDateString(date: Date): string {
		if(date == null) {
			return "";
		}
	
		return this.monthNames[date.getMonth()] + " " + date.getDate() + " " + date.getFullYear();
	}
}