import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { HttpModule } from '@angular/http';
import { IoTDevice } from '../models/iot-device';
import { Telemetry } from '../models/telemetry';

@Injectable()
export class DeviceService{
    private endpointUrl = 'api/Device';  // URL to web api
    
    constructor(private http: Http) { }
    
    registerDevice(): Observable<IoTDevice> {
        return this.http
            .post(this.endpointUrl + `/RegisterDevice`, { })
            .map(response => response.json() as IoTDevice)
            .catch(this.handleError);
    }

    postTelemetry(telemetry: Telemetry) {
        return this.http
            .post(this.endpointUrl + `/PostTelemetry`, telemetry)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.log(error); // for demo purposes only

        return Promise.reject(error.message || error);
    }
}