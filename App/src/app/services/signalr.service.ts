import { Injectable } from '@angular/core';

import { SignalR, ISignalRConnection, SignalRConnection, BroadcastEventListener } from 'ng2-signalr';
import { Telemetry } from '../models/telemetry';

@Injectable()
export class SignalRService {

    public onTelemetryUpdate$ = new BroadcastEventListener<Telemetry>('TelemetryUpdate');
    //public onReaderStatusUpdate$ = new BroadcastEventListener<ReaderStatus>('ReaderStatusUpdate');

    constructor(private signalR: SignalR) { 
      this.connect();
    }

    public connect(): void {
      this.signalR.connect().then((c) => {
        c.listen(this.onTelemetryUpdate$);
        //c.listen(this.onReaderStatusUpdate$);
      })
    }
}
