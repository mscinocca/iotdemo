﻿using IoTDemo.Models;
using IoTDemo.SignalR;
using Microsoft.AspNet.SignalR;
using System;
using System.Web.Http;

namespace IoTDemo.Controllers
{
   [RoutePrefix("api/Device")]
   public class DeviceController : ApiController
   {
      [HttpPost]
      [Route("RegisterDevice")]
      public IHttpActionResult RegisterDevice()
      {
         try
         {
            var random = new Random();

            var device = new IoTDevice
            {
               DeviceId = random.Next(1, 100000).ToString()
            };

            return Ok(device);
         }
         catch (Exception e)
         {
            return InternalServerError(e);
         }
      }

      [HttpPost]
      [Route("PostTelemetry")]
      public IHttpActionResult PostTelemetry(Telemetry telemetry)
      {
         try
         {
            telemetry.DateTime = telemetry.DateTime.AddMilliseconds(-telemetry.DateTime.Millisecond);

            var context = GlobalHost.ConnectionManager.GetHubContext<IoTHub>();
            context.Clients.All.TelemetryUpdate(telemetry);

            return Ok();
         }
         catch (Exception e)
         {
            return InternalServerError(e);
         }
      }
   }
}
