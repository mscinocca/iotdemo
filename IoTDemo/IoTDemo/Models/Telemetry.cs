﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IoTDemo.Models
{
   public class Telemetry
   {
      public string DeviceId { get; set; }

      public DateTime DateTime { get; set; }
      public int Temperature { get; set; }
      public int SeismicActivity { get; set; }
   }
}