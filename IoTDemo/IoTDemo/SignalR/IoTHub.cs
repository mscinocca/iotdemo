﻿using IoTDemo.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IoTDemo.SignalR
{
   [HubName("iotHub")]
   public class IoTHub : Hub
   {
      public void TelemetryUpdate(Telemetry telemetry)
      {
         Clients.All.TelemetryUpdate(telemetry);
      }
   }
}