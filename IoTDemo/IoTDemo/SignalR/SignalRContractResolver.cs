﻿using Microsoft.AspNet.SignalR.Infrastructure;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace IoTDemo.SignalR
{
   public class SignalRContractResolver : IContractResolver
   {

      private readonly Assembly assembly;
      private readonly IContractResolver defaultContractSerializer;

      public SignalRContractResolver()
      {
         defaultContractSerializer = new DefaultContractResolver();
         assembly = typeof(Connection).Assembly;
      }

      public JsonContract ResolveContract(Type type)
      {
         return defaultContractSerializer.ResolveContract(type);
      }
   }
}