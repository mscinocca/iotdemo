﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Newtonsoft.Json;
using IoTDemo.SignalR;
using Newtonsoft.Json.Converters;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin.Cors;

[assembly: OwinStartup(typeof(IoTDemo.Startup))]

namespace IoTDemo
{
   public partial class Startup
   {
      public void Configuration(IAppBuilder app)
      {
         var settings = new JsonSerializerSettings();
         settings.ContractResolver = new SignalRContractResolver();

         settings.Converters.Add(new IsoDateTimeConverter());
         settings.Converters.Add(new StringEnumConverter());

         var serializer = JsonSerializer.Create(settings);
         GlobalHost.DependencyResolver.Register(typeof(JsonSerializer), () => serializer);

         GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(110);
         GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromSeconds(60);

         GlobalHost.Configuration.DefaultMessageBufferSize = 1;

         // This value must be no more than 1/3 of the DisconnectTimeout value.
         GlobalHost.Configuration.KeepAlive = TimeSpan.FromSeconds(20);

         var hubConfiguration = new HubConfiguration
         {
            EnableDetailedErrors = false,
         };

         app.UseCors(CorsOptions.AllowAll);

         app.MapSignalR(hubConfiguration);

         ConfigureAuth(app);
      }
   }
}
